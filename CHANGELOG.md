# Changelog #
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] ##

### Added ###

### Changed ###

### Fixed ###

## [1.0.8] - 2025-01-20 ##

### Changed ###

- PHP 8.3 property static types
- Upgrade dependent packages
- Upgrade license to 2025

## [1.0.7] - 2024-09-01 ##

### Changed ###

- Upgrade dependent packages

### Fixed ###

- Phpstan extension.neon of composer/pcre missing by dg/composer-cleaner

## [1.0.6] - 2024-05-19 ##

### Changed ###

- Update SECURITY key
- Upgrade dependent packages

## [1.0.5] - 2023-12-29 ##

### Changed ###

- Increase minimal PHP to 8.3
- Update security contacts
- Upgrade dependent packages
- Upgrade license to 2024

## [1.0.4] - 2023-03-12 ##

### Changed ###

- Update dependencies to newer version

### Fixed ###

- Minor changes due to new version of code checker

## [1.0.3] - 2022-12-30 ##

### Changed ###

- Upgrade license to 2023
- Increase minimal PHP to 8.2
- Upgrade dependent packages
- Add forgotten `ExtendedExceptionTrait` extension to `IllegalActionException`

## [1.0.2] - 2022-10-04 ##

### Changed ###

- Extended and translatable exceptions

## [1.0.1] - 2022-09-15 ##

### Changed ###

- Update dependencies to newer version

## [1.0.0] - 2022-07-20 ##

### Added ###

- Parser basics
- Tokenizer basics
