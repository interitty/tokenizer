<?php

declare(strict_types=1);

namespace Interitty\Tokenizer;

class Token
{
    /** All available token type constants */
    public const string TOKEN_END = 'end';

    /** @var string */
    protected string $lexeme;

    /** @var int */
    protected int $line;

    /** @var int */
    protected int $position;

    /** @var string */
    protected string $type;

    /** @var string|null */
    protected ?string $value = null;

    /**
     * Constructor
     *
     * @param string $type
     * @param string $lexeme
     * @param int $line
     * @param int $position
     * @return void
     */
    public function __construct(string $type, string $lexeme, int $line, int $position)
    {
        $this->setType($type);
        $this->setLexeme($lexeme);
        $this->setLine($line);
        $this->setPosition($position);
    }

    /**
     * Value processor
     *
     * @return string
     */
    protected function processValue(): string
    {
        $value = $this->getLexeme();
        return $value;
    }
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">

    /**
     * Lexeme getter
     *
     * @return string
     */
    public function getLexeme(): string
    {
        return $this->lexeme;
    }

    /**
     * Lexeme setter
     *
     * @param string $lexeme
     * @return static Provides fluent interface
     */
    protected function setLexeme(string $lexeme): static
    {
        $this->lexeme = $lexeme;
        return $this;
    }

    /**
     * Line getter
     *
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * Line setter
     *
     * @param int $line
     * @return static Provides fluent interface
     */
    protected function setLine(int $line): static
    {
        $this->line = $line;
        return $this;
    }

    /**
     * Position getter
     *
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * Position setter
     *
     * @param int $position
     * @return static Provides fluent interface
     */
    protected function setPosition(int $position): static
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Type getter
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Type setter
     *
     * @param string $type
     * @return static Provides fluent interface
     */
    protected function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Value getter
     *
     * @return string
     */
    public function getValue(): string
    {
        if ($this->value === null) {
            $value = $this->processValue();
            $this->value = $value;
        }
        return $this->value;
    }
    // </editor-fold>
}
