<?php

declare(strict_types=1);

namespace Interitty\Tokenizer;

use Interitty\Tokenizer\Exceptions\IllegalActionException;
use Interitty\Tokenizer\Exceptions\UnexpectedTokenException;

abstract class BaseTokenizerParser extends BaseParser
{
    /** @var Tokenizer */
    protected Tokenizer $tokenizer;

    /**
     * Parses given string
     *
     * @param string $string
     * @return mixed
     */
    public function parse(string $string): mixed
    {
        try {
            $this->setTokenizer($this->createTokenizer($string));
            return $this->processParse();
        } catch (IllegalActionException $exception) {
            $currentToken = $this->current();
            throw new UnexpectedTokenException($currentToken, $exception);
        }
    }

    /**
     * CurrentTokenType processor
     *
     * @return string
     */
    protected function currentTokenType(): string
    {
        return $this->current()->getType();
    }

    /**
     * CurrentTokenLexeme processor
     *
     * @return string
     */
    protected function currentTokenLexeme(): string
    {
        return $this->current()->getValue();
    }
    // <editor-fold defaultstate="collapsed" desc="Factories">

    /**
     * Tokenizer factory
     *
     * @param string $string
     * @return Tokenizer
     */
    protected function createTokenizer(string $string): Tokenizer
    {
        $tokenizer = new Tokenizer($string);
        return $tokenizer;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">

    /**
     * Token checker
     *
     * @param string $type
     * @param string|null $value [OPTIONAL]
     * @return bool
     */
    protected function check(string $type, ?string $value = null): bool
    {
        $check = ($this->currentTokenType() === $type) &&
            (($value === null) || ($this->currentTokenLexeme() === $value));
        return $check;
    }

    /**
     * Current token getter
     *
     * @return Token
     */
    protected function current(): Token
    {
        $current = $this->getTokenizer()->current();
        return $current;
    }

    /**
     * Token expect check
     *
     * @param string $type
     * @param string $value [OPTIONAL]
     * @return Tokenizer
     */
    protected function expect(string $type, string $value = null): Tokenizer
    {
        $token = $this->current();
        $tokenizer = $this->getTokenizer()->expect($token, $type, $value);
        return $tokenizer;
    }

    /**
     * Next token getter
     *
     * @return void
     */
    protected function next(): void
    {
        $this->getTokenizer()->next();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">

    /**
     * Tokenizer getter
     *
     * @return Tokenizer
     */
    protected function getTokenizer(): Tokenizer
    {
        return $this->tokenizer;
    }

    /**
     * Tokenizer setter
     *
     * @param Tokenizer $tokenizer
     * @return static Provides fluent interface
     */
    protected function setTokenizer(Tokenizer $tokenizer): static
    {
        $this->tokenizer = $tokenizer;
        return $this;
    }
    // </editor-fold>
}
