<?php

declare(strict_types=1);

namespace Interitty\Tokenizer;

use Interitty\Tokenizer\Exceptions\UnexpectedTokenException;
use Interitty\Utils\Strings;

use function array_key_exists;
use function array_shift;
use function assert;
use function count;
use function end;
use function in_array;
use function is_array;

class Tokenizer
{
    /** @var string[] Mapping from token regex patterns to token classes */
    public array $map = [];

    /** @var Token|null Current token */
    protected ?Token $current = null;

    /** @var int Current line of string to tokenize */
    protected int $line = 1;

    /** @var int Current position on current line of string to tokenize */
    protected int $position = 1;

    /** @var string[] */
    protected array $skippedTokenTypes = [];

    /** @var string String to tokenize */
    protected string $string = '';

    /** @var Token[] Buffered tokens */
    protected array $tokens = [];

    /**
     * Constructor
     *
     * @param string $string
     * @param int $line [OPTIONAL]
     * @return void
     */
    public function __construct(string $string, int $line = 1)
    {
        $this->setLine($line);
        $this->setString($string);
    }

    /**
     * Current token getter
     *
     * @return Token
     */
    public function current(): Token
    {
        if ($this->hasCurrent() === false) {
            $this->next();
        }
        return $this->getCurrent();
    }

    /**
     * Token expect check
     *
     * @param Token $token
     * @param string $type
     * @param string $value [OPTIONAL]
     * @return static Provides fluent interface
     */
    public function expect(Token $token, string $type, string $value = null): static
    {
        if (($token->getType() !== $type) || (($value !== null) && ($token->getValue() !== $value))) {
            throw new UnexpectedTokenException($token);
        }
        return $this;
    }

    /**
     * Next token getter
     *
     * @return Token
     */
    public function next(): Token
    {
        $skippedTokenTypes = $this->getSkippedTokenTypes();
        do {
            $string = $this->getString();
            $token = $this->getNextToken();
            if (($token === null) && ($string !== '')) {
                $token = $this->processNextToken($string);
            }
            if ($token === null) {
                $token = $this->createToken(Token::TOKEN_END);
            }
            $tokenType = $token->getType();
        } while (($tokenType !== Token::TOKEN_END) && (in_array($tokenType, $skippedTokenTypes, true) === true));

        $this->setCurrent($token);
        return $token;
    }

    /**
     * Match next token processor
     *
     * @param string $string
     * @return Token|null
     */
    protected function processNextToken(string $string): ?Token
    {
        $token = null;
        foreach ($this->map as $type => $pattern) {
            /** @phpstan-var array{0: string, 1?: string}|null $matches */
            $matches = Strings::match($string, $pattern);
            if (is_array($matches) === true) {
                $lexeme = array_key_exists(1, $matches) ? $matches[1] : $matches[0];
                $token = $this->createToken($type, $lexeme);
                $this->processToken($token, $string);
                break;
            }
        }

        return $token;
    }

    /**
     * Token processor
     *
     * @param Token $token
     * @param string $string
     * @return void
     */
    protected function processToken(Token $token, string $string): void
    {
        $lexeme = $this->processTokenLexeme($token);
        /** @phpstan-var string[] $lines */
        $lines = Strings::split($lexeme, '~\r?\n|\r~');
        $linesCount = count($lines);

        if ($linesCount > 1) {
            $currentLine = $this->getLine();
            $this->setLine($currentLine + $linesCount - 1);
            $line = (string) end($lines);
            $this->setPosition(Strings::length($line) + 1);
        } else {
            $position = $this->getPosition();
            $this->setPosition($position + Strings::length($lexeme));
        }

        $this->setString(Strings::substring($string, Strings::length($lexeme)));
    }

    /**
     * Token lexeme processor
     *
     * @param Token $token
     * @return string
     */
    protected function processTokenLexeme(Token $token): string
    {
        $lexeme = $token->getLexeme();
        return $lexeme;
    }

    // <editor-fold defaultstate="collapsed" desc="Factories">
    /**
     * Token factory
     *
     * @param string $type
     * @param string $lexeme [OPTIONAL]
     * @param int $increasePosition [OPTIONAL]
     * @return Token
     */
    protected function createToken(string $type, string $lexeme = '', int $increasePosition = 0): Token
    {
        $line = $this->getLine();
        $position = $this->getPosition() + $increasePosition;
        $token = new Token($type, $lexeme, $line, $position);
        return $token;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Current getter
     *
     * @return Token
     */
    protected function getCurrent(): Token
    {
        if ($this->hasCurrent() === false) {
            $token = $this->next();
            $this->setCurrent($token);
        }
        assert($this->current instanceof Token);
        return $this->current;
    }

    /**
     * Current checker
     *
     * @return bool
     */
    protected function hasCurrent(): bool
    {
        $hasCurrent = ($this->current instanceof Token);
        return $hasCurrent;
    }

    /**
     * Current setter
     *
     * @param Token $current
     * @return static Provides fluent interface
     */
    protected function setCurrent(Token $current): static
    {
        $this->current = $current;
        return $this;
    }

    /**
     * Line getter
     *
     * @return int
     */
    protected function getLine(): int
    {
        return $this->line;
    }

    /**
     * Line setter
     *
     * @param int $line
     * @return static Provides fluent interface
     */
    protected function setLine(int $line): static
    {
        $this->line = $line;
        return $this;
    }

    /**
     * Position getter
     *
     * @return int
     */
    protected function getPosition(): int
    {
        return $this->position;
    }

    /**
     * Position setter
     *
     * @param int $position
     * @return static Provides fluent interface
     */
    protected function setPosition(int $position): static
    {
        $this->position = $position;
        return $this;
    }

    /**
     * SkippedTokenType adder
     *
     * @param string $skippedTokenType
     * @return static Provides fluent interface
     */
    public function addSkippedTokenType(string $skippedTokenType): static
    {
        $this->skippedTokenTypes[] = $skippedTokenType;

        return $this;
    }

    /**
     * SkippedTokenTypes getter
     *
     * @return string[]
     */
    public function getSkippedTokenTypes(): array
    {
        return $this->skippedTokenTypes;
    }

    /**
     * SkippedTokenTypes setter
     *
     * @param string[] $skippedTokenTypes
     * @return static Provides fluent interface
     */
    public function setSkippedTokenTypes(array $skippedTokenTypes): static
    {
        $this->skippedTokenTypes = [];
        foreach ($skippedTokenTypes as $skippedTokenType) {
            $this->addSkippedTokenType($skippedTokenType);
        }
        return $this;
    }

    /**
     * String getter
     *
     * @return string
     */
    protected function getString(): string
    {
        return $this->string;
    }

    /**
     * String setter
     *
     * @param string $string
     * @return static Provides fluent interface
     */
    protected function setString(string $string): static
    {
        $this->string = $string;
        return $this;
    }

    /**
     * Token adder
     *
     * @param Token $token
     * @return static Provides fluent interface
     */
    protected function addToken(Token $token): static
    {
        $this->tokens[] = $token;
        return $this;
    }

    /**
     * Next token getter
     *
     * @return Token|null
     */
    protected function getNextToken(): ?Token
    {
        return array_shift($this->tokens);
    }

    /**
     * Tokens getter
     *
     * @return Token[]
     */
    protected function getTokens(): array
    {
        return $this->tokens;
    }

    /**
     * Tokens setter
     *
     * @param Token[] $tokens
     * @return static Provides fluent interface
     */
    protected function setTokens(array $tokens): static
    {
        $this->tokens = [];
        foreach ($tokens as $token) {
            $this->addToken($token);
        }
        return $this;
    }

    // </editor-fold>
}
