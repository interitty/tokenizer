<?php

declare(strict_types=1);

namespace Interitty\Tokenizer;

use Interitty\Tokenizer\Exceptions\IllegalActionException;

use function array_pop;
use function array_push;
use function array_splice;
use function call_user_func;
use function count;
use function end;
use function is_callable;
use function range;

abstract class BaseParser
{
    /** @var int[] */
    protected array $productionsEqual = [];

    /** @var int[] */
    protected array $productionsLefts = [];

    /** @var int[] */
    protected array $productionsLengths = [];

    /** @var int[] */
    protected array $table = [];

    /** @var int */
    protected int $tablePitch = 0;

    /** @var int[] */
    protected array $terminalsTypes = [];

    /** @var int[] */
    protected array $terminalsValues = [];

    /**
     * Current processor
     *
     * @return mixed
     */
    abstract protected function current(): mixed;

    /**
     * CurrentTokenType processor
     *
     * @return string|null
     */
    abstract protected function currentTokenType(): ?string;

    /**
     * CurrentTokenLexeme processor
     *
     * @return string|null
     */
    abstract protected function currentTokenLexeme(): ?string;

    /**
     * Next processor
     *
     * @return void
     */
    abstract protected function next(): void;

    /**
     * Parse processor
     *
     * @return mixed
     */
    protected function processParse(): mixed
    {
        $stack = [null, 0];
        do {
            $action = $this->processParseAction($stack);
            if ($action > 0) { // => shift
                array_push($stack, $this->currentTokenLexeme());
                array_push($stack, $action);
                $this->next();
            } elseif ($action < 0) { // => reduce
                $popped = array_splice($stack, count($stack) - ($this->productionsLengths[-$action] * 2));
                $arguments = $this->processParseArguments($popped, $action);
                /** @phpstan-var int $state */
                $state = end($stack);
                $goto = $this->table[$state * $this->tablePitch + $this->productionsLefts[-$action]];
                array_push($stack, $this->processReduce(-$action, $arguments));
                array_push($stack, $goto);
            }
        } while ($action !== 0);

        array_pop($stack); // go away, state!
        return array_pop($stack);
    }

    /**
     * Reduce processor
     *
     * @param int $action
     * @param mixed[] $arguments
     * @return mixed
     */
    protected function processReduce(int $action, array $arguments): mixed
    {
        if (isset($this->productionsEqual[$action]) === true) {
            $action = $this->productionsEqual[$action];
        }
        $reduceCallback = [$this, 'processReduce' . $action];
        $reduced = is_callable($reduceCallback) ? call_user_func($reduceCallback, $arguments) : null;
        return $reduced;
    }

    /**
     * Parse arguments processor
     *
     * @param mixed[] $popped
     * @param int $action
     * @return mixed[]
     */
    protected function processParseArguments(array $popped, int $action): array
    {
        $arguments = [];
        if ($this->productionsLengths[-$action] > 0) {
            foreach (range(0, ($this->productionsLengths[-$action] - 1) * 2, 2) as $index) {
                $arguments[$index / 2 + 1] = $popped[$index];
            }
        }
        return $arguments;
    }

    /**
     * Parse action processor
     *
     * @param mixed[] $stack
     * @return int
     */
    protected function processParseAction(array $stack): int
    {
        /** @phpstan-var int $state */
        $state = end($stack);
        $terminal = 0;
        $currentTokenType = $this->currentTokenType();
        if (isset($this->terminalsTypes[$currentTokenType])) {
            $terminal = $this->terminalsTypes[$currentTokenType];
        } else {
            $currentTokenLexeme = $this->currentTokenLexeme();
            if (isset($this->terminalsValues[$currentTokenLexeme])) {
                $terminal = $this->terminalsValues[$currentTokenLexeme];
            }
        }

        $tableIndex = $state * $this->tablePitch + $terminal;
        if (isset($this->table[$tableIndex]) !== true) {
            throw new IllegalActionException('Illegal action');
        }

        $action = $this->table[$tableIndex];
        return $action;
    }
}
