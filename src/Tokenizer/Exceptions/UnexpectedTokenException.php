<?php

declare(strict_types=1);

namespace Interitty\Tokenizer\Exceptions;

use Interitty\Exceptions\ExtendedExceptionInterface;
use Interitty\Exceptions\ExtendedExceptionTrait;
use Interitty\Tokenizer\Token;
use LogicException;
use Throwable;

class UnexpectedTokenException extends LogicException implements ExtendedExceptionInterface
{
    use ExtendedExceptionTrait;

    /** @var Token */
    protected Token $token;

    /**
     * Constructor
     *
     * @param Token $token
     * @param Throwable|null $previous [OPTIONAL]
     * @return void
     */
    public function __construct(Token $token, ?Throwable $previous = null)
    {
        $message = 'Unexpected token ":token" of type ":type" on line :line at position :position';
        parent::__construct($message, 0, $previous);
        $this->setToken($token);
        $this->setData([]);
        $this->setMessage($message);
        $this->setData([
            'token' => $token->getLexeme(),
            'type' => $token->getType(),
            'line' => $token->getLine(),
            'position' => $token->getPosition(),
        ]);
    }
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">

    /**
     * Token getter
     *
     * @return Token
     */
    public function getToken(): Token
    {
        return $this->token;
    }

    /**
     * Token setter
     *
     * @param Token $token
     * @return static Provides fluent interface
     */
    protected function setToken(Token $token): static
    {
        $this->token = $token;
        return $this;
    }
    // </editor-fold>
}
