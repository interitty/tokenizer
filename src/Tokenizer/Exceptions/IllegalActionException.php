<?php

declare(strict_types=1);

namespace Interitty\Tokenizer\Exceptions;

use Interitty\Exceptions\ExtendedExceptionInterface;
use Interitty\Exceptions\ExtendedExceptionTrait;
use LogicException;

class IllegalActionException extends LogicException implements ExtendedExceptionInterface
{
    use ExtendedExceptionTrait;
}
