<?php

declare(strict_types=1);

namespace Interitty\Tokenizer;

use Generator;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Tokenizer\Exceptions\IllegalActionException;
use PHPUnit\Framework\MockObject\MockObject;

use function array_merge;

/**
 * @coversDefaultClass Interitty\Tokenizer\BaseParser
 */
class BaseParserTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Parse processor implementation of no content test
     *
     * @return void
     * @covers ::processParse
     */
    public function testProcessParseEmpty(): void
    {
        $baseParser = $this->createBaseParserMock(['processParseAction']);
        $baseParser->expects(self::once())
            ->method('processParseAction')
            ->with(self::equalTo([null, 0]))
            ->willReturn(0);
        self::assertNull($this->callNonPublicMethod($baseParser, 'processParse'));
    }

    /**
     * Parse processor implementation of reduce action test
     *
     * @return void
     * @covers ::processParse
     */
    public function testProcessParseReduce(): void
    {
        $action = 3;
        $reduceOutput = 'output';
        $baseParser = $this->createBaseParserMock(['processParseAction', 'processParseArguments', 'processReduce']);
        $this->setNonPublicPropertyValue($baseParser, 'productionsLefts', [$action => 0]);
        $this->setNonPublicPropertyValue($baseParser, 'productionsLengths', [$action => 0]);
        $this->setNonPublicPropertyValue($baseParser, 'table', [0 => 0]);
        $baseParser->expects(self::any())->method('processParseAction')
            ->willReturnCallback(static fn(array $stack) => match ([$stack]) { // @phpstan-ignore-line
                    [[null, 0]] => -$action,
                    [[null, 0, 'output', 0]] => 0,
            });
        $baseParser->expects(self::once())
            ->method('processParseArguments')
            ->with(self::equalTo([]), self::equalTo(-$action))
            ->willReturn([]);
        $baseParser->expects(self::once())
            ->method('processReduce')
            ->with(self::equalTo($action), self::equalTo([]))
            ->willReturn($reduceOutput);
        self::assertSame($reduceOutput, $this->callNonPublicMethod($baseParser, 'processParse'));
    }

    /**
     * Parse processor implementation of shift action test
     *
     * @return void
     * @covers ::processParse
     */
    public function testProcessParseShift(): void
    {
        $output = 'output';
        $baseParser = $this->createBaseParserMock(['processParseAction']);
        $baseParser->expects(self::exactly(2))
            ->method('processParseAction')
            ->willReturnOnConsecutiveCalls(1, 0);
        $baseParser->expects(self::exactly(1))
            ->method('currentTokenLexeme')
            ->willReturn($output);
        self::assertSame($output, $this->callNonPublicMethod($baseParser, 'processParse'));
    }

    /**
     * Reduce processor implementation test
     *
     * @return void
     * @covers ::processReduce
     */
    public function testProcessReduce(): void
    {
        $action = 1;
        $arguments = ['arguments'];
        $reduceCallbackMethod = 'processReduce' . $action;
        $output = 'output';
        $baseParser = $this->createBaseParserMock([], [$reduceCallbackMethod]);
        $baseParser->expects(self::exactly(1))
            ->method($reduceCallbackMethod)
            ->with(self::equalTo($arguments))
            ->willReturn($output);
        self::assertSame($output, $this->callNonPublicMethod($baseParser, 'processReduce', [$action, $arguments]));
    }

    /**
     * Reduce equal processor implementation test
     *
     * @return void
     * @covers ::processReduce
     */
    public function testProcessReduceEqual(): void
    {
        $action = 2;
        $equalAction = 1;
        $arguments = ['arguments'];
        $equalReduceMethod = 'processReduce' . $equalAction;
        $reduceMethod = 'processReduce' . $action;
        $output = 'output';
        $baseParser = $this->createBaseParserMock([], [$reduceMethod, $equalReduceMethod]);
        $this->setNonPublicPropertyValue($baseParser, 'productionsEqual', [$action => $equalAction]);
        $baseParser->expects(self::exactly(0))
            ->method($reduceMethod);
        $baseParser->expects(self::exactly(1))
            ->method($equalReduceMethod)
            ->with(self::equalTo($arguments))
            ->willReturn($output);
        self::assertSame($output, $this->callNonPublicMethod($baseParser, 'processReduce', [$action, $arguments]));
    }

    /**
     * ProcessParseArguments DataProvider
     *
     * @return Generator<string, array{0: int[], 1: int, 2: int[], 3: int[]}>
     */
    public function processParseArgumentsDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="productions lenghts 0">
        yield 'productions lenghts 0' => [[], 1, [-1 => 0], []];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="productions lenghts 1">
        yield 'productions lenghts 1' => [[0 => 2], 1, [-1 => 1], [1 => 2]];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="productions lenghts 2">
        yield 'productions lenghts 2' => [[0 => 2, 2 => 4], 1, [-1 => 2], [1 => 2, 2 => 4]];
        // </editor-fold>
    }

    /**
     * Parse arguments processor implementation test
     *
     * @param int[] $popped
     * @param int $action
     * @param int[] $lengths
     * @param int[] $arguments
     * @return void
     * @dataProvider processParseArgumentsDataProvider
     * @covers ::processParseArguments
     */
    public function testProcessParseArguments(array $popped, int $action, array $lengths, array $arguments): void
    {
        $baseParser = $this->createBaseParserMock([]);
        $this->setNonPublicPropertyValue($baseParser, 'productionsLengths', $lengths);
        $processedArguments = $this->callNonPublicMethod($baseParser, 'processParseArguments', [$popped, $action]);
        self::assertSame($arguments, $processedArguments);
    }

    /**
     * Parse action process with illegal action implementation test
     *
     * @return void
     * @covers ::processParseAction
     * @group negative
     */
    public function testProcessParseActionIllegalAction(): void
    {
        $stack = [1];
        $table = [];

        $baseParser = $this->createBaseParserMock([]);
        $this->setNonPublicPropertyValue($baseParser, 'table', $table);
        $this->setNonPublicPropertyValue($baseParser, 'tablePitch', 1);
        $this->setNonPublicPropertyValue($baseParser, 'terminalsTypes', [Token::TOKEN_END => 1]);
        $baseParser->expects(self::once())
            ->method('currentTokenType')
            ->willReturn(Token::TOKEN_END);

        $this->expectException(IllegalActionException::class);
        $this->expectExceptionMessage('Illegal action');
        $this->callNonPublicMethod($baseParser, 'processParseAction', [$stack]);
    }

    /**
     * Parse action process with no terminal implementation test
     *
     * @return void
     * @covers ::processParseAction
     */
    public function testProcessParseActionNoTerminal(): void
    {
        $stack = [1];
        $action = 1;
        $table = [1 => $action];
        $tablePitch = 1;

        $baseParser = $this->createBaseParserMock([]);
        $this->setNonPublicPropertyValue($baseParser, 'table', $table);
        $this->setNonPublicPropertyValue($baseParser, 'tablePitch', $tablePitch);
        $this->setNonPublicPropertyValue($baseParser, 'terminalsTypes', []);
        $this->setNonPublicPropertyValue($baseParser, 'terminalsValues', []);
        $baseParser->expects(self::once())
            ->method('currentTokenType')
            ->willReturn('string');
        $baseParser->expects(self::once())
            ->method('currentTokenLexeme')
            ->willReturn('test');
        self::assertSame($action, $this->callNonPublicMethod($baseParser, 'processParseAction', [$stack]));
    }

    /**
     * Parse action process with terminal type implementation test
     *
     * @return void
     * @covers ::processParseAction
     */
    public function testProcessParseActionTerminalType(): void
    {
        $stack = [1];
        $action = 2;
        $table = [2 => $action];

        $baseParser = $this->createBaseParserMock([]);
        $this->setNonPublicPropertyValue($baseParser, 'table', $table);
        $this->setNonPublicPropertyValue($baseParser, 'tablePitch', 1);
        $this->setNonPublicPropertyValue($baseParser, 'terminalsTypes', [Token::TOKEN_END => 1]);
        $baseParser->expects(self::once())
            ->method('currentTokenType')
            ->willReturn(Token::TOKEN_END);
        self::assertSame($action, $this->callNonPublicMethod($baseParser, 'processParseAction', [$stack]));
    }

    /**
     * Parse action process with terminal value implementation test
     *
     * @return void
     * @covers ::processParseAction
     */
    public function testProcessParseActionTerminalValue(): void
    {
        $stack = [1];
        $action = 3;
        $table = [3 => $action];

        $baseParser = $this->createBaseParserMock([]);
        $this->setNonPublicPropertyValue($baseParser, 'table', $table);
        $this->setNonPublicPropertyValue($baseParser, 'tablePitch', 1);
        $this->setNonPublicPropertyValue($baseParser, 'terminalsTypes', []);
        $this->setNonPublicPropertyValue($baseParser, 'terminalsValues', [';' => 2]);
        $baseParser->expects(self::once())
            ->method('currentTokenType')
            ->willReturn('string');
        $baseParser->expects(self::once())
            ->method('currentTokenLexeme')
            ->willReturn(';');
        self::assertSame($action, $this->callNonPublicMethod($baseParser, 'processParseAction', [$stack]));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * BaseParser mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @param string[] $addMethods [OPTIONAL] List of mocked method names
     * @return BaseParser|MockObject
     */
    protected function createBaseParserMock(array $methods = [], array $addMethods = []): MockObject
    {
        $mockMethods = array_merge($methods, ['current', 'currentTokenType', 'currentTokenLexeme', 'next']);
        $mock = $this->createMockAbstract(BaseParser::class, $mockMethods, $addMethods);
        return $mock;
    }

    // </editor-fold>
}
