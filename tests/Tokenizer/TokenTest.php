<?php

declare(strict_types=1);

namespace Interitty\Tokenizer;

use Interitty\PhpUnit\BaseTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Tokenizer\Token
 */
class TokenTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">

    /**
     * Tester of constructor
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $type = 'type';
        $lexeme = 'lexeme';
        $line = 42;
        $position = 43;
        $token = new Token($type, $lexeme, $line, $position);

        self::assertSame($type, $token->getType());
        self::assertSame($lexeme, $token->getLexeme());
        self::assertSame($line, $token->getLine());
        self::assertSame($position, $token->getPosition());
    }

    /**
     * Tester of lexeme getter/setter implementation
     *
     * @param string $lexeme
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::getLexeme
     * @covers ::setLexeme
     */
    public function testGetSetLexeme(string $lexeme): void
    {
        $this->processTestGetSet(Token::class, 'lexeme', $lexeme);
    }

    /**
     * Tester of line getter/setter implementation
     *
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::getLine
     * @covers ::setLine
     */
    public function testGetSetLine(): void
    {
        $this->processTestGetSet(Token::class, 'line', 42);
    }

    /**
     * Tester of position getter/setter implementation
     *
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::getPosition
     * @covers ::setPosition
     */
    public function testGetSetPosition(): void
    {
        $this->processTestGetSet(Token::class, 'position', 42);
    }

    /**
     * Tester of type getter/setter implementation
     *
     * @param string $type
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::getType
     * @covers ::setType
     */
    public function testGetSetType(string $type): void
    {
        $this->processTestGetSet(Token::class, 'type', $type);
    }

    /**
     * Tester of value getter implementation
     *
     * @return void
     * @covers ::getValue
     */
    public function testGetValue(): void
    {
        $value = 'value';
        $token = $this->createTokenMock(['processValue']);
        $token->expects(self::once())
            ->method('processValue')
            ->willReturn($value);

        self::assertSame($value, $token->getValue());
        self::assertSame($value, $token->getValue());
    }

    /**
     * Tester of value processor implementation
     *
     * @return void
     * @covers ::processValue
     */
    public function testProcessValue(): void
    {
        $lexeme = 'lexeme';
        $token = $this->createTokenMock(['getLexeme']);
        $token->expects(self::exactly(2))
            ->method('getLexeme')
            ->willReturn($lexeme);

        self::assertSame($lexeme, $this->callNonPublicMethod($token, 'processValue'));
        self::assertSame($lexeme, $this->callNonPublicMethod($token, 'processValue'));
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">

    /**
     * Token mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Token&MockObject
     */
    protected function createTokenMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(Token::class, $methods);
        return $mock;
    }
    // </editor-fold>
}
