<?php

declare(strict_types=1);

namespace Interitty\Tokenizer;

use Interitty\PhpUnit\BaseTestCase;

use const PHP_EOL;

/**
 * @coversDefaultClass Interitty\Tokenizer\Tokenizer
 */
class TokenizerIntegrationTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">

    /**
     * Tester of token stream implementation
     *
     * @return void
     * @covers ::next
     * @covers ::processNextToken
     * @covers ::processToken
     */
    public function testTokenStream(): void
    {
        $tokenizer = $this->createTokenizer('say 123');
        $tokenizer->map = [
            'number' => '~^\d+~',
            'whitespace' => '~^\s+~',
            'string' => '~^\w+~',
        ];

        self::assertSame('string', $tokenizer->next()->getType());
        self::assertSame('say', $tokenizer->current()->getValue());
        self::assertSame(1, $tokenizer->current()->getPosition());

        self::assertSame('whitespace', $tokenizer->next()->getType());
        self::assertSame(' ', $tokenizer->current()->getValue());
        self::assertSame(4, $tokenizer->current()->getPosition());

        self::assertSame('number', $tokenizer->next()->getType());
        self::assertSame('123', $tokenizer->current()->getValue());
        self::assertSame(5, $tokenizer->current()->getPosition());

        self::assertSame(Token::TOKEN_END, $tokenizer->next()->getType());
        self::assertSame(1, $tokenizer->current()->getLine());
        self::assertSame(8, $tokenizer->current()->getPosition());
    }

    /**
     * Tester of token stream iteration implementation
     *
     * @return void
     * @covers ::next
     * @covers ::current
     */
    public function testTokenStreamIteration(): void
    {
        $tokenizer = $this->createTokenizer('say 123');
        $tokenizer->addSkippedTokenType('whitespace');
        $tokenizer->addSkippedTokenType(Token::TOKEN_END);
        $tokenizer->map = [
            'number' => '~^\d+~',
            'whitespace' => '~^\s+~',
            'string' => '~^\w+~',
        ];

        $string = '';
        do {
            $token = $tokenizer->next();
            $string .= $token->getValue();
            self::assertSame($token, $tokenizer->current());
        } while ($token->getType() !== Token::TOKEN_END);
        self::assertSame('say123', $string);
    }

    /**
     * Tester of token stream for empty string implementation
     *
     * @return void
     * @covers ::next
     * @covers ::processNextToken
     */
    public function testTokenStreamEmptyString(): void
    {
        $tokenizer = $this->createTokenizer('');

        self::assertSame(Token::TOKEN_END, $tokenizer->next()->getType());
        self::assertSame(1, $tokenizer->current()->getLine());
        self::assertSame(1, $tokenizer->current()->getPosition());
    }

    /**
     * Tester of token stream for empty map implementation
     *
     * @return void
     * @covers ::next
     * @covers ::processNextToken
     */
    public function testTokenStreamEmptyMap(): void
    {
        $tokenizer = $this->createTokenizer('say 123');
        $tokenizer->map = [];

        self::assertSame(Token::TOKEN_END, $tokenizer->next()->getType());
        self::assertSame(1, $tokenizer->current()->getLine());
        self::assertSame(1, $tokenizer->current()->getPosition());
    }

    /**
     * Tester of token stream for multiline string implementation
     *
     * @return void
     * @covers ::next
     * @covers ::processNextToken
     * @covers ::processToken
     */
    public function testTokenStreamMultiline(): void
    {
        $block = '{' . PHP_EOL . 'some code' . PHP_EOL . '}';
        $string = $block . $block;
        $tokenizer = $this->createTokenizer($string);
        $tokenizer->map = [
            'block' => '~^{[^}]+}~',
        ];

        self::assertSame('block', $tokenizer->next()->getType());
        self::assertSame($block, $tokenizer->current()->getValue());
        self::assertSame(1, $tokenizer->current()->getLine());
        self::assertSame(1, $tokenizer->current()->getPosition());

        self::assertSame('block', $tokenizer->next()->getType());
        self::assertSame($block, $tokenizer->current()->getValue());
        self::assertSame(3, $tokenizer->current()->getLine());
        self::assertSame(2, $tokenizer->current()->getPosition());

        self::assertSame(Token::TOKEN_END, $tokenizer->next()->getType());
        self::assertSame(5, $tokenizer->current()->getLine());
        self::assertSame(2, $tokenizer->current()->getPosition());
    }

    /**
     * Tester of token expections implementation
     *
     * @return void
     * @covers ::expect
     * @covers ::next
     * @covers ::processNextToken
     * @covers ::processToken
     */
    public function testTokenExpections(): void
    {
        $code = 'some code';
        $string = '{' . $code . '}';
        $tokenizer = $this->createTokenizer($string);
        $tokenizer->map = [
            'brackets' => '~^[{}]~',
            'code' => '~^[^{}]+~',
        ];

        self::assertSame($tokenizer, $tokenizer->expect($tokenizer->next(), 'brackets', '{'));
        self::assertSame($tokenizer, $tokenizer->expect($tokenizer->next(), 'code'));
        self::assertSame($code, $tokenizer->current()->getValue());
        self::assertSame($tokenizer, $tokenizer->expect($tokenizer->next(), 'brackets', '}'));
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">

    /**
     * Tokenizer mock factory
     *
     * @param string $string
     * @param int $line [OPTIONAL]
     * @return Tokenizer
     */
    protected function createTokenizer(string $string, int $line = 1): Tokenizer
    {
        $tokenizer = new Tokenizer($string, $line);
        return $tokenizer;
    }
    // </editor-fold>
}
