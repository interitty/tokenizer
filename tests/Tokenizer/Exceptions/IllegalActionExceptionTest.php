<?php

declare(strict_types=1);

namespace Interitty\Tokenizer\Exceptions;

use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Tokenizer\Exceptions\IllegalActionException
 */
class IllegalActionExceptionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">

    /**
     * Tester of constructor
     *
     * @return void
     */
    public function testConstructor(): void
    {
        $message = 'message';

        $this->expectException(IllegalActionException::class);
        $this->expectExceptionMessage($message);
        throw new IllegalActionException($message);
    }
    // </editor-fold>
}
