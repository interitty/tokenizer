<?php

declare(strict_types=1);

namespace Interitty\Tokenizer\Exceptions;

use Interitty\PhpUnit\BaseTestCase;
use Interitty\Tokenizer\Token;
use PHPUnit\Framework\MockObject\MockObject;
use Throwable;

/**
 * @coversDefaultClass Interitty\Tokenizer\Exceptions\UnexpectedTokenException
 */
class UnexpectedTokenExceptionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of constructor
     *
     * @return void
     * @covers ::__construct
     * @covers ::getToken
     * @covers ::setToken
     */
    public function testConstructor(): void
    {
        $data = [
            'token' => ($lexeme = 'lexeme'),
            'type' => ($type = 'type'),
            'line' => ($line = 42),
            'position' => ($position = 43),
        ];
        $token = new Token($type, $lexeme, $line, $position);
        $previous = $this->createThrowableMock();

        $this->expectException(UnexpectedTokenException::class);
        $this->expectExceptionMessage('Unexpected token ":token" of type ":type" on line :line at position :position');
        $this->expectExceptionData($data);
        $this->expectExceptionCallback(static function ($exception) use ($previous, $token): void {
            /** @phpstan-var UnexpectedTokenException $exception */
            self::assertSame($token, $exception->getToken());
            self::assertSame($previous, $exception->getPrevious());
        });

        throw new UnexpectedTokenException($token, $previous);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Token factory
     *
     * @param string $type
     * @param string $lexeme
     * @param int $line
     * @param int $position
     * @return Token
     */
    protected function createToken(string $type, string $lexeme, int $line, int $position): Token
    {
        $token = new Token($type, $lexeme, $line, $position);
        return $token;
    }

    /**
     * Throwable mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Throwable|MockObject
     */
    protected function createThrowableMock(array $methods = []): MockObject
    {
        $mock = $this->createMockAbstract(Throwable::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
