<?php

declare(strict_types=1);

namespace Interitty\Tokenizer;

use Generator;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\Tokenizer\Exceptions\IllegalActionException;
use Interitty\Tokenizer\Exceptions\UnexpectedTokenException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Tokenizer\BaseTokenizerParser
 */
class BaseTokenizerParserTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of tokenizer factory implementation
     *
     * @param string $string
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::createTokenizer
     * @group integration
     */
    public function testCreateTokenizer(string $string): void
    {
        $tokenizerParser = $this->createBaseTokenizerParserMock([]);
        $tokenizer = $this->callNonPublicMethod($tokenizerParser, 'createTokenizer', [$string]);
        self::assertSame($string, $this->callNonPublicMethod($tokenizer, 'getString'));
    }

    /**
     * Tester of parse implementation
     *
     * @param string $string
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::parse
     * @group integration
     */
    public function testParse(string $string): void
    {
        $output = 'output';
        $tokenizerParser = $this->createBaseTokenizerParserMock(['processParse']);
        $tokenizerParser->expects(self::once())
            ->method('processParse')
            ->willReturn($output);
        self::assertSame($output, $tokenizerParser->parse($string));
        $tokenizer = $this->callNonPublicMethod($tokenizerParser, 'getTokenizer');
        self::assertSame($string, $this->callNonPublicMethod($tokenizer, 'getString'));
    }

    /**
     * Tester of parse implementation
     *
     * @return void
     * @covers ::parse
     * @group negative
     */
    public function testParseUnexpectedToken(): void
    {
        $currentToken = $this->createToken('type', 'lexeme', 1, 1);
        $exception = $this->createIllegalActionExceptionMock();
        $tokenizerParser = $this->createBaseTokenizerParserMock(['current', 'processParse']);
        $tokenizerParser->expects(self::once())
            ->method('current')
            ->willReturn($currentToken);
        $tokenizerParser->expects(self::once())
            ->method('processParse')
            ->willThrowException($exception);

        $this->expectException(UnexpectedTokenException::class);
        $this->expectExceptionMessage('Unexpected token ":token" of type ":type" on line :line at position :position');
        $this->expectExceptionData(['token' => 'lexeme', 'type' => 'type', 'line' => 1, 'position' => 1]);
        $this->expectExceptionCallback(static function ($exception) use ($currentToken): void {
            /** @phpstan-var UnexpectedTokenException $exception */
            self::assertSame($currentToken, $exception->getToken());
        });

        $tokenizerParser->parse('');
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Check DataProvider
     *
     * @return Generator<string, array{0: string, 1: string|null, 2: bool, 3: int}>
     */
    public function checkDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="Same type and no value">
        yield 'Same type and no value' => ['string', null, true, 0];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Same type and same value">
        yield 'Same type and same value' => ['string', 'value', true, 1];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Same type and different value">
        yield 'Same type and different value' => ['string', '12345', false, 1];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Different type and no value">
        yield 'Different type and no value' => ['int', null, false, 0];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Different type and same value">
        yield 'Different type and same value' => ['int', 'value', false, 0];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Different type and different value">
        yield 'Different type and different value' => ['int', '12345', false, 0];
        // </editor-fold>
    }

    /**
     * Tester of check implementation
     *
     * @param string $type
     * @param string|null $value
     * @param bool $check
     * @param int $valueCheckCount
     * @return void
     * @dataProvider checkDataProvider
     * @covers ::check
     */
    public function testCheck(string $type, ?string $value, bool $check, int $valueCheckCount): void
    {
        $tokenizerParser = $this->createBaseTokenizerParserMock(['currentTokenType', 'currentTokenLexeme']);
        $tokenizerParser->expects(self::once())
            ->method('currentTokenType')
            ->willReturn('string');
        $tokenizerParser->expects(self::exactly($valueCheckCount))
            ->method('currentTokenLexeme')
            ->willReturn('value');
        self::assertSame($check, $this->callNonPublicMethod($tokenizerParser, 'check', [$type, $value]));
    }

    /**
     * Tester of current implementation
     *
     * @return void
     * @covers ::current
     */
    public function testCurrent(): void
    {
        $token = $this->createTokenMock();
        $tokenizer = $this->createTokenizerMock(['current']);
        $tokenizer->expects(self::once())
            ->method('current')
            ->willReturn($token);
        $tokenizerParser = $this->createBaseTokenizerParserMock(['getTokenizer']);
        $tokenizerParser->expects(self::once())
            ->method('getTokenizer')
            ->willReturn($tokenizer);
        self::assertSame($token, $this->callNonPublicMethod($tokenizerParser, 'current'));
    }

    /**
     * Tester of currentTokenLexeme and currentTokenType implementation
     *
     * @return void
     * @covers ::currentTokenLexeme
     * @covers ::currentTokenType
     */
    public function testCurrentTokenLexeme(): void
    {
        $tokenType = Token::TOKEN_END;
        $tokenValue = 'tokenValue';
        $token = $this->createTokenMock(['getType', 'getValue']);
        $token->expects(self::once())
            ->method('getType')
            ->willReturn($tokenType);
        $token->expects(self::once())
            ->method('getValue')
            ->willReturn($tokenValue);
        $tokenizerParser = $this->createBaseTokenizerParserMock(['current']);
        $tokenizerParser->expects(self::exactly(2))
            ->method('current')
            ->willReturn($token);
        self::assertSame($tokenValue, $this->callNonPublicMethod($tokenizerParser, 'currentTokenLexeme'));
        self::assertSame($tokenType, $this->callNonPublicMethod($tokenizerParser, 'currentTokenType'));
    }

    /**
     * Tester of expect implementation
     *
     * @return void
     * @covers ::expect
     */
    public function testExpect(): void
    {
        $type = 'type';
        $value = 'value';

        $token = $this->createTokenMock(['getValue']);
        $tokenizer = $this->createTokenizerMock(['expect']);
        $tokenizerParser = $this->createBaseTokenizerParserMock(['current', 'getTokenizer']);
        $tokenizerParser->expects(self::once())
            ->method('current')
            ->willReturn($token);
        $tokenizerParser->expects(self::once())
            ->method('getTokenizer')
            ->willReturn($tokenizer);

        $tokenizer->expects(self::once())
            ->method('expect')
            ->with(self::equalTo($token), self::equalTo($type), self::equalTo($value))
            ->willReturnSelf();
        self::assertSame($tokenizer, $this->callNonPublicMethod($tokenizerParser, 'expect', [$type, $value]));
    }

    /**
     * Tester of Tokenizer getter/setter implementation
     *
     * @return void
     * @covers ::getTokenizer
     * @covers ::setTokenizer
     */
    public function testGetSetTokenizer(): void
    {
        $tokenizer = $this->createTokenizerMock();
        $this->processTestGetSet(BaseTokenizerParser::class, 'tokenizer', $tokenizer);
    }

    /**
     * Tester of next implementation
     *
     * @return void
     * @covers ::next
     */
    public function testNext(): void
    {
        $tokenizer = $this->createTokenizerMock(['next']);
        $tokenizer->expects(self::once())
            ->method('next');
        $tokenizerParser = $this->createBaseTokenizerParserMock(['getTokenizer']);
        $tokenizerParser->expects(self::once())
            ->method('getTokenizer')
            ->willReturn($tokenizer);
        $this->callNonPublicMethod($tokenizerParser, 'next');
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * BaseTokenizerParser mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return BaseTokenizerParser|MockObject
     */
    protected function createBaseTokenizerParserMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(BaseTokenizerParser::class, $methods);
        return $mock;
    }

    /**
     * IllegalActionException mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return IllegalActionException|MockObject
     */
    protected function createIllegalActionExceptionMock(array $methods = []): IllegalActionException
    {
        $mock = $this->createPartialMock(IllegalActionException::class, $methods);
        return $mock;
    }

    /**
     * Token factory
     *
     * @param string $type
     * @param string $lexeme
     * @param int $line
     * @param int $position
     * @return Token
     */
    protected function createToken(string $type, string $lexeme, int $line, int $position): Token
    {
        $token = new Token($type, $lexeme, $line, $position);
        return $token;
    }

    /**
     * Token mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Token|MockObject
     */
    protected function createTokenMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(Token::class, $methods);
        return $mock;
    }

    /**
     * Tokenizer mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Tokenizer|MockObject
     */
    protected function createTokenizerMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(Tokenizer::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
