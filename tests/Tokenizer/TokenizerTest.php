<?php

declare(strict_types=1);

namespace Interitty\Tokenizer;

use Interitty\PhpUnit\BaseTestCase;
use Interitty\Tokenizer\Exceptions\UnexpectedTokenException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\Tokenizer\Tokenizer
 */
class TokenizerTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">

    /**
     * Tester of constructor
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $string = 'string';
        $line = 42;
        $tokenizer = $this->createTokenizer($string, $line);

        self::assertSame($string, $this->callNonPublicMethod($tokenizer, 'getString'));
        self::assertSame($line, $this->callNonPublicMethod($tokenizer, 'getLine'));
    }

    /**
     * Tester of token factory
     *
     * @return void
     * @covers ::createToken
     */
    public function testCreateToken(): void
    {
        $increasePosition = 42;
        $lexeme = 'lexeme';
        $line = 1;
        $position = 2;
        $type = 'type';
        $tokenizer = $this->createTokenizerMock(['getLine', 'getPosition']);
        $tokenizer->expects(self::once())
            ->method('getLine')
            ->willReturn($line);
        $tokenizer->expects(self::once())
            ->method('getPosition')
            ->willReturn($position);

        /* @var $token Token */
        $token = $this->callNonPublicMethod($tokenizer, 'createToken', [$type, $lexeme, $increasePosition]);
        self::assertSame($type, $token->getType());
        self::assertSame($lexeme, $token->getLexeme());
        self::assertSame($line, $token->getLine());
        self::assertSame($position + $increasePosition, $token->getPosition());
    }

    /**
     * Tester of current method implementation
     *
     * @return void
     * @covers ::current
     */
    public function testCurrent(): void
    {
        $current = $this->createTokenMock();
        $tokenizer = $this->createTokenizerMock(['getCurrent']);
        $tokenizer->expects(self::once())
            ->method('getCurrent')
            ->willReturn($current);

        self::assertSame($current, $tokenizer->current());
    }

    /**
     * Tester of expect implementation
     *
     * @return void
     * @covers ::expect
     */
    public function testExpect(): void
    {
        $type = 'tokenType';
        $current = $this->createToken($type, 'lexeme', 1, 1);
        $tokenizer = $this->createTokenizerMock();

        self::assertSame($tokenizer, $tokenizer->expect($current, $type));
    }

    /**
     * Tester of expect wrong lexeme implementation
     *
     * @return void
     * @covers ::expect
     */
    public function testExpectWrongLexeme(): void
    {
        $type = 'tokenType';
        $current = $this->createToken($type, 'wrongLexeme', 1, 1);
        $tokenizer = $this->createTokenizerMock();

        $this->expectException(UnexpectedTokenException::class);
        $this->expectExceptionMessage('Unexpected token ":token" of type ":type" on line :line at position :position');
        $this->expectExceptionData(['token' => 'wrongLexeme', 'type' => 'tokenType', 'line' => 1, 'position' => 1]);
        $tokenizer->expect($current, $type, 'lexeme');
    }

    /**
     * Tester of expect wrong type implementation
     *
     * @return void
     * @covers ::expect
     */
    public function testExpectWrongType(): void
    {
        $current = $this->createToken('wrongType', 'lexeme', 1, 1);
        $tokenizer = $this->createTokenizerMock();

        $this->expectException(UnexpectedTokenException::class);
        $this->expectExceptionMessage('Unexpected token ":token" of type ":type" on line :line at position :position');
        $this->expectExceptionData(['token' => 'lexeme', 'type' => 'wrongType', 'line' => 1, 'position' => 1]);
        $tokenizer->expect($current, 'tokenType');
    }

    /**
     * Tester of nextToken getter implementation
     *
     * @return void
     * @covers ::getNextToken
     */
    public function testGetNextToken(): void
    {
        $tokens = [];
        $tokens[0] = $this->createTokenMock();
        $tokens[1] = $this->createTokenMock();
        $tokens[2] = $this->createTokenMock();
        $tokenizer = $this->createTokenizerMock();

        self::assertSame($tokenizer, $this->callNonPublicMethod($tokenizer, 'setTokens', [$tokens]));
        self::assertSame($tokens[0], $this->callNonPublicMethod($tokenizer, 'getNextToken'));
        self::assertSame($tokens[1], $this->callNonPublicMethod($tokenizer, 'getNextToken'));
        self::assertSame($tokens[2], $this->callNonPublicMethod($tokenizer, 'getNextToken'));
    }

    /**
     * Tester of current getter/setter/checker implementation
     *
     * @return void
     * @covers ::getCurrent
     * @covers ::hasCurrent
     * @covers ::setCurrent
     */
    public function testeGetSetCurrent(): void
    {
        $token = $this->createTokenMock();
        $tokenizer = $this->createTokenizerMock(['next']);
        $tokenizer->expects(self::once())
            ->method('next')
            ->willReturn($token);

        self::assertFalse($this->callNonPublicMethod($tokenizer, 'hasCurrent'));
        self::assertSame($token, $this->callNonPublicMethod($tokenizer, 'getCurrent'));
        self::assertSame($token, $this->callNonPublicMethod($tokenizer, 'getCurrent'));
        self::assertTrue($this->callNonPublicMethod($tokenizer, 'hasCurrent'));
    }

    /**
     * Tester of line getter/setter implementation
     *
     * @return void
     * @covers ::getLine
     * @covers ::setLine
     */
    public function testGetSetLine(): void
    {
        $line = 42;
        $this->processTestGetSet(Tokenizer::class, 'line', $line);
    }

    /**
     * Tester of position getter/setter implementation
     *
     * @return void
     * @covers ::getPosition
     * @covers ::setPosition
     */
    public function testGetSetPosition(): void
    {
        $position = 42;
        $this->processTestGetSet(Tokenizer::class, 'position', $position);
    }

    /**
     * Tester of skippedTokenTypes adder/getter/setter implementation
     *
     * @return void
     * @covers ::addSkippedTokenType
     * @covers ::getSkippedTokenTypes
     * @covers ::setSkippedTokenTypes
     */
    public function testGetSetSkippedTokenTypes(): void
    {
        $skippedTokenTypes = ['type_one', 'type_two'];
        $tokenizer = $this->createTokenizerMock();

        self::assertCount(0, $tokenizer->getSkippedTokenTypes());
        self::assertSame($tokenizer, $tokenizer->setSkippedTokenTypes($skippedTokenTypes));
        self::assertSame($skippedTokenTypes, $tokenizer->getSkippedTokenTypes());
        self::assertSame($tokenizer, $tokenizer->addSkippedTokenType('type_three'));
        self::assertCount(3, $tokenizer->getSkippedTokenTypes());
    }

    /**
     * Tester of string getter/setter implementation
     *
     * @param string $string
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::getString
     * @covers ::setString
     */
    public function testGetSetString(string $string): void
    {
        $this->processTestGetSet(Tokenizer::class, 'string', $string);
    }

    /**
     * Tester of token adder/getter/setter implementation
     *
     * @return void
     * @covers ::addToken
     * @covers ::getTokens
     * @covers ::setTokens
     */
    public function testGetSetToken(): void
    {
        $token = $this->createTokenMock();
        $tokens = [];
        $tokens[] = $token;
        $tokens[] = $token;
        $tokenizer = $this->createTokenizerMock();

        self::assertCount(0, $this->callNonPublicMethod($tokenizer, 'getTokens'));
        self::assertSame($tokenizer, $this->callNonPublicMethod($tokenizer, 'setTokens', [$tokens]));
        self::assertSame($tokens, $this->callNonPublicMethod($tokenizer, 'getTokens'));
        self::assertSame($tokenizer, $this->callNonPublicMethod($tokenizer, 'addToken', [$token]));
        self::assertCount(3, $this->callNonPublicMethod($tokenizer, 'getTokens'));
    }

    /**
     * Tester of TokenLexeme processor
     *
     * @return void
     * @covers ::processTokenLexeme
     */
    public function testProcesTokenLexeme(): void
    {
        $lexeme = 'lexeme';
        $token = $this->createTokenMock(['getLexeme']);
        $token->expects(self::once())
            ->method('getLexeme')
            ->willReturn($lexeme);
        $tokenizer = $this->createTokenizerMock();

        self::assertSame($lexeme, $this->callNonPublicMethod($tokenizer, 'processTokenLexeme', [$token]));
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">

    /**
     * Token factory
     *
     * @param string $type
     * @param string $lexeme
     * @param int $line
     * @param int $position
     * @return Token
     */
    protected function createToken(string $type, string $lexeme, int $line, int $position): Token
    {
        $token = new Token($type, $lexeme, $line, $position);
        return $token;
    }

    /**
     * Token mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Token&MockObject
     */
    protected function createTokenMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(Token::class, $methods);
        return $mock;
    }

    /**
     * Tokenizer mock factory
     *
     * @param string $string
     * @param int $line [OPTIONAL]
     * @return Tokenizer
     */
    protected function createTokenizer(string $string, int $line = 1): Tokenizer
    {
        $tokenizer = new Tokenizer($string, $line);
        return $tokenizer;
    }

    /**
     * Tokenizer mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Tokenizer&MockObject
     */
    protected function createTokenizerMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(Tokenizer::class, $methods);
        return $mock;
    }
    // </editor-fold>
}
